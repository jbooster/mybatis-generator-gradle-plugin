package org.jbooster.mybatis.generator

import org.gradle.api.Project

/**
 * @author jearton
 * @since 2017/8/15
 */
class MybatisGeneratorExtension {
    // 是否覆盖上一次生成的文件
    def overwrite = true

    // 是否打印明细日志
    def verbose = false

    // 是否打印调试日志
    def debug = false

    // key：配置文件路径（在src/main/resources下的相对路径）
    // value：上下文
    def config = [:] as Map<String, ConfigFileContext>
}

// 数据库连接信息，在xml中的引用方式：${jdbc.属性名}
// 如果使用mysql，建议只配置本地url，其它使用默认即可
class JdbcExtension {
    def driver = 'com.mysql.jdbc.Driver'
    def url
    def username = 'root'
    def password = ''
}

// 待生成的文件路径信息，在xml中的引用方式：${file.属性名}
// 不建议自定义配置，使用默认即可
class ConfigFileContext {
    // 实体类文件
    def modelDir = 'gensrc/main/java'
    def modelPackage

    // *Mapper.xml文件
    def xmlDir = 'src/main/resources'
    def xmlPackage = 'mappers'

    // *Mapper.java文件
    def clientDir = 'src/main/java'
    def clientPackage

    ConfigFileContext(Project project) {
        modelPackage = project.group + '.model.pojos'
        clientPackage = project.group + '.mappers'
    }
}