package org.jbooster.mybatis.generator

import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.tasks.TaskAction
import org.mybatis.generator.api.MyBatisGenerator
import org.mybatis.generator.api.ProgressCallback
import org.mybatis.generator.api.VerboseProgressCallback
import org.mybatis.generator.config.xml.ConfigurationParser
import org.mybatis.generator.internal.DefaultShellCallback
import org.mybatis.generator.internal.NullProgressCallback

/**
 * @author jearton
 * @since 2017/8/15
 */
class MyBatisGeneratorTask extends DefaultTask {

    final static LOG_PREFIX = '[MBG]'

    final static DEFAULT_CONFIG_FILE = "generatorConfig.xml"

    // 配置文件上下文：共享
    final static JDBC_DRIVER = 'jdbc.driver'
    final static JDBC_URL = 'jdbc.url'
    final static JDBC_USERNAME = 'jdbc.username'
    final static JDBC_PASSWORD = 'jdbc.password'

    // 配置文件上下文：配置文件隔离
    final static FILE_BASE_PACKAGE = 'file.basePackage'
    final static FILE_MODEL_DIR = 'file.modelDir'
    final static FILE_MODEL_PACKAGE = 'file.modelPackage'
    final static FILE_XML_DIR = 'file.xmlDir'
    final static FILE_XML_PACKAGE = 'file.xmlPackage'
    final static FILE_CLIENT_DIR = 'file.clientDir'
    final static FILE_CLIENT_PACKAGE = 'file.clientPackage'

    @TaskAction
    void generatorAction() {
        println "${LOG_PREFIX} BEGIN"

        // 读取配置参数
        def generatorExtension = this.generatorExtension

        // 遍历配置文件
        generatorExtension.config.each {
            def configFile = project.file("src/main/resources/$it.key")
            def xmlContext = this.xmlContext(it.value)
            if (generatorExtension.verbose) {
                xmlContext.each { println "${LOG_PREFIX} XML[$configFile.name] CONTEXT: [$it.key = $it.value]" }
            }

            // 验证配置参数
            validate(configFile, xmlContext)

            // 执行官方MBG
            generate(configFile, xmlContext, generatorExtension)
        }

        println "${LOG_PREFIX} END"
    }

    MybatisGeneratorExtension getGeneratorExtension() {
        def generatorExtension = new MybatisGeneratorExtension()
        generatorExtension.overwrite = project.mbg.overwrite
        generatorExtension.verbose = project.mbg.verbose
        generatorExtension.debug = project.mbg.debug
        def config = (project.extensions.mbg.config as Map<String, Closure>).collectEntries {
            def context = new ConfigFileContext(project)
            it.value.delegate = context
            it.value()
            [it.key, context]
        } as Map<String, ConfigFileContext>
        if (config.size() > 0) {
            generatorExtension.config = config
        } else {
            generatorExtension.config = [(DEFAULT_CONFIG_FILE): new ConfigFileContext(project)]
        }
        generatorExtension
    }

    /**
     * 配置文件中的上下文参数
     */
    Map<?, ?> xmlContext(ConfigFileContext context) {
        def jdbc = project.extensions.mbg.jdbc as JdbcExtension
        [
                (JDBC_DRIVER)        : jdbc.driver,
                (JDBC_URL)           : jdbc.url,
                (JDBC_USERNAME)      : jdbc.username,
                (JDBC_PASSWORD)      : jdbc.password,
                (FILE_BASE_PACKAGE)  : project.group,
                (FILE_MODEL_DIR)     : context.modelDir,
                (FILE_MODEL_PACKAGE) : context.modelPackage,
                (FILE_XML_DIR)       : context.xmlDir,
                (FILE_XML_PACKAGE)   : context.xmlPackage,
                (FILE_CLIENT_DIR)    : context.clientDir,
                (FILE_CLIENT_PACKAGE): context.clientPackage
        ].findAll { it.value != null }
    }

    static void validate(File generatorConfigFile, Map<?, ?> xmlContext) {
        if (!generatorConfigFile.exists()) {
            throw new GradleException("configFile[$generatorConfigFile.absolutePath] not exists")
        }

        if (!xmlContext[JDBC_DRIVER]) {
            println xmlContext[JDBC_DRIVER]
            throw new GradleException("\${$JDBC_DRIVER} may not be empty")
        }

        if (!xmlContext[JDBC_URL]) {
            throw new GradleException("\${$JDBC_URL} may not be empty")
        }

        if (!xmlContext[JDBC_USERNAME]) {
            throw new GradleException("\${$JDBC_USERNAME} may not be empty")
        }

        if (!xmlContext[FILE_BASE_PACKAGE]) {
            throw new GradleException("\${$FILE_BASE_PACKAGE} may not be empty, please set project.group=you package")
        }

        if (!xmlContext[FILE_MODEL_PACKAGE]) {
            throw new GradleException("\${$FILE_MODEL_PACKAGE} may not be empty")
        }

        if (!xmlContext[FILE_XML_PACKAGE]) {
            throw new GradleException("\${$FILE_XML_PACKAGE} may not be empty")
        }

        if (!xmlContext[FILE_CLIENT_PACKAGE]) {
            throw new GradleException("\${$FILE_CLIENT_PACKAGE} may not be empty")
        }
    }

    void generate(File generatorConfigFile, Map<?, ?> xmlContext, MybatisGeneratorExtension generatorExtension) {
        // 当前执行环境下的父级目录
        def projectDir = project.projectDir.path.replace(new File("").absolutePath, '.') + File.separator
        if (!new File(projectDir).exists()) {
            throw new GradleException("projectDir[$projectDir] not exists")
        }

        // 初始化配置
        def extra = new Properties()
        extra.putAll(xmlContext)
        def warnings = []
        def config = new ConfigurationParser(extra, warnings).parseConfiguration(generatorConfigFile)
        config.contexts.each {
            it.javaClientGeneratorConfiguration.targetProject = projectDir + it.javaClientGeneratorConfiguration.targetProject
            it.javaModelGeneratorConfiguration.targetProject = projectDir + it.javaModelGeneratorConfiguration.targetProject
            it.sqlMapGeneratorConfiguration.targetProject = projectDir + it.sqlMapGeneratorConfiguration.targetProject
        }

        // 执行过程钩子
        ProgressCallback progressCallback = generatorExtension.verbose ? new VerboseProgressCallback() {
            @Override
            void startTask(String taskName) {
                println "${LOG_PREFIX} ${taskName}"
            }
        } : new NullProgressCallback()
        def shellCallback = new DefaultShellCallback(generatorExtension.overwrite)

        // 执行生成器
        new MyBatisGenerator(config, shellCallback, warnings).generate(progressCallback)

        // 打印执行过程警告
        if (generatorExtension.debug) {
            warnings.each { println "${LOG_PREFIX} [WARNING] ${it}" }
        }
    }
}
