package org.jbooster.mybatis.generator

import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * @author jearton
 * @since 2017/2/26
 */
class MyBatisGeneratorPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        project.extensions.create("mbg", MybatisGeneratorExtension)
        project.mbg.extensions.create("jdbc", JdbcExtension)
        project.task("mbg", type: MyBatisGeneratorTask)
    }
}