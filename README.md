# mybatis-generator-gradle-plugin
## Introduction
Mybatis Generator（以下简称MBG）官方地址：http://www.mybatis.org/generator/

MBG官方提供了多种运行方式，包含：命令行、Java编码、Ant任务、Maven插件、Eclipse插件，
唯独没有Gradle插件，而目前网上开源的MBG-Gradle插件都是基于Ant Task。本项目基于Java编码定义了Gradle任务，
并自适应父子项目这种结构，即可以在父项目执行task。

## Quick Start
- 引入插件
    ```groovy
    plugins {
        id 'org.jbooster.mybatis.generator' version '0.1.3'
    }
    ```
    
- 参数配置
    ```groovy
    mbg {
        // 是否覆盖旧文件，默认为true
        overwrite = true
      
        // 是否打印执行过程日志，默认为false
        verbose = false
      
        // 是否打印调试日志，默认为false
        debug = false
      
        // 数据库连接信息，在xml中的引用方式：${jdbc.属性名}
        // 若该信息已在xml文件中写死，则不必在此处配置
        jdbc {
            // 驱动类，默认为'com.mysql.jdbc.Driver'
            driver = 'com.mysql.jdbc.Driver'
            // 默认为空
            url = 'jdbc:mysql://127.0.0.1:3306/xxx?useSSL=false'
            // 默认为root
            username = 'root' 
            // 默认为空
            password = '****'
        }
      
        // 配置文件路径（默认包含generatorConfig.xml）及上下文，支持多配置文件
        // key：配置文件路径（在src/main/resources下的相对路径）
        // value：上下文
        config['generatorConfig.xml'] = {
          // 待生成的文件路径信息，在xml中的引用方式：${file.属性名}
          // 以下皆为默认配置，不建议自定义配置，推荐默认配置
          // 若在此处配该信息已在xml文件中写死，则不必设置，使用空闭包即可
          
          // 实体类文件
          modelDir = 'gensrc/main/java'
          modelPackage = "${your project package}.model.pojo"
                      
           // *Mapper.xml文件
           xmlDir = 'src/main/resources'
           xmlPackage = 'mapper'
           
           // *Mapper.java文件
           clientDir = 'src/main/java'
           clientPackage = "${your project package}.mapper"
        }
    }
    ```

- 示例配置
    ```groovy
    mbg {
        verbose = true
        jdbc {
            url = 'jdbc:mysql://127.0.0.1:3306/user?useSSL=false'
            username = 'root'
            password = ''
        }
    }
    ```
    
- 执行task
    ```bash
    gradle mbg
    ```

## generatorConfig.xml示例
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE generatorConfiguration PUBLIC "-//mybatis.org//DTD MyBatis Generator Configuration 1.0//EN"
        "http://mybatis.org/dtd/mybatis-generator-config_1_0.dtd">

<generatorConfiguration>
    <!-- 指定数据库环境 -->
    <context id="Mysql" targetRuntime="MyBatis3" defaultModelType="flat">
        <property name="autoDelimitKeywords" value="true"/>
        <property name="beginningDelimiter" value="`"/>
        <property name="endingDelimiter" value="`"/>

        <!-- 自定义XML格式化器（可选） -->
        <property name="xmlFormatter" value="org.mybatis.generator.api.dom.DefaultXmlFormatter"/>

        <!-- 自定义插件（可选） -->
        <plugin type="org.mybatis.generator.plugins.CachePlugin"/>

        <!-- 自定义注释生成器（可选） -->
        <commentGenerator type="org.mybatis.generator.internal.DefaultCommentGenerator"/>

        <!-- 数据库连接 -->
        <jdbcConnection driverClass="${jdbc.driver}" connectionURL="${jdbc.url}" userId="${jdbc.username}"
                        password="${jdbc.password}">
        </jdbcConnection>

        <!-- 文件路径：实体 -->
        <javaModelGenerator targetProject="${file.modelDir}" targetPackage="${file.modelPackage}"/>

        <!-- 文件路径：xxxMapper.xml -->
        <sqlMapGenerator targetProject="${file.xmlDir}" targetPackage="${file.xmlPackage}"/>

        <!-- 文件路径：xxxMapper.java（可选） -->
        <javaClientGenerator targetProject="${file.clientDir}" targetPackage="${file.clientPackage}" type="XMLMAPPER"/>

        <!-- 要生成的关系表 -->
        <table tableName="tc_order" domainObjectName="Order">
            <columnOverride column="status" javaType="xx.xx.xx.OrderStatus"/>
        </table>
        <table tableName="tc_event" domainObjectName="Event">
            <generatedKey column="id" sqlStatement="JDBC"/>
        </table>
    </context>
</generatorConfiguration>
```